package sortingclosure;

import java.io.IOException;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.TreeSet;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;



public class PairClosure {

	public static class TokenizerMapper
    extends Mapper<Object, Text, Text, Text>{
		private Text ky = new Text();
	    private Text val = new Text();
	    public void map(Object key, Text value, Context context) 
		 throws IOException, InterruptedException {
     StringTokenizer itr = new StringTokenizer(value.toString(), ",");
     while (itr.hasMoreTokens()) {
     ky.set(itr.nextToken());
     val.set(itr.nextToken());
     context.write(ky, val);
     context.write(val, ky);
     		}
	    }
	}

	
	public static class SortClosure extends Reducer<Text, Text, Text, Text>{
			protected void reduce(Text key, Iterable<Text> value, Context context)throws IOException, InterruptedException {
			TreeSet<Pair> tree = new TreeSet<Pair>();
			for(Text val:value) {
			Pair temp = new Pair();
			temp.setKey(context.getCurrentKey().toString());
			temp.setValue(val.toString());
			tree.add(temp);
			}
			TreeSet<Pair> newTree = new TreeSet<Pair>();
			TreeSet<Pair> tempSet = new TreeSet<Pair>();
			Iterator<Pair> tempItr = tempSet.iterator();
			boolean allGroupsClosed=false;
			while (!allGroupsClosed) {
				String prevKey = "";
				String currkey="";
				allGroupsClosed = true;
				Iterator<Pair> itr = tree.iterator();
				while(itr.hasNext()){
					Pair currPair = itr.next();
					// get key of current pair being processed
					currkey = currPair.getKey();
					if (!currkey.equals(prevKey) || !itr.hasNext()) {
						if (currkey.equals(prevKey) && !itr.hasNext())	tempSet.add(currPair);
						if (!currkey.equals(prevKey) && !itr.hasNext() && (currkey.compareTo(currPair.getValue())<=0)) tempSet.add(currPair);
						if (!prevKey.equals("")) {
							Pair firstPair = tempSet.first();
							String firstKey = firstPair.getKey();
							String firstValue = firstPair.getValue();
							if (firstKey.compareTo(firstValue) > 0) {
								if (tempSet.size() > 1) {
									allGroupsClosed = false;
									tempItr = tempSet.iterator();
									Pair grpPair = tempItr.next();
									while (tempItr.hasNext()) {
										grpPair = tempItr.next();
										Pair newPair = new Pair();
										newPair.setKey(firstValue);
										newPair.setValue(grpPair.getValue());
										newTree.add(newPair);
										newTree.add(newPair.reversePair());
									}
									// Decide if first pair should be added
									grpPair = tempSet.last();
									String lastValue = grpPair.getValue();
									if (firstKey.compareTo(lastValue)<0) {
										newTree.add(firstPair);
									}
								}
							} else {
								// First pair not reversed, move key group as is to newTree
								newTree.addAll(tempSet);
									}
				}
						tempSet.clear();

			}
					tempSet.add(currPair);
					// change previous key to current key for next pair to process
					prevKey = currkey;
			
			
	        }
				tree.clear();
				tree.addAll(newTree);
				newTree.clear();
				
			}
			Iterator<Pair> outItr = tree.iterator();
			while(outItr.hasNext()){
				Pair outPair= outItr.next();
				context.write(new Text(outPair.getKey()), new Text(outPair.getValue()));
			}
		}
	}

	public static void main(String[] args) throws Exception {
	    Configuration conf = new Configuration();
	    Job job = Job.getInstance(conf, "mapandreduce");
	    job.setJarByClass(PairClosure.class);
	    job.setMapperClass(TokenizerMapper.class);
	    job.setCombinerClass(SortClosure.class);
	    job.setReducerClass(SortClosure.class);
	    job.setOutputKeyClass(Text.class);
	    job.setOutputValueClass(Text.class);
	    FileInputFormat.addInputPath(job, new Path(args[0]));
	    FileOutputFormat.setOutputPath(job, new Path(args[1]));
	    System.exit(job.waitForCompletion(true) ? 0 : 1);
	  }
	
	}




